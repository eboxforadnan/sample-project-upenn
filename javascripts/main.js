$(function() {

	$.getJSON('data/images.json', function(data) {
	
		var template = $('#uphotostpl').html(); 
		//converting date string to standard format using moment libraries However forsomereason it is not displaying I am trying to find the way 
		var nd=moment(data.datetime).format('MMMM Do YYYY, h:mm:ss a');
		//ultizing Mustache libraries to render data through custom template 
		var tml = Mustache.to_html(template, data, nd); 
	    $('#gallerysuperbox').html(tml);
	
	}); //getJSON   
}); // This is function End 